<?php

$amount = (isset($_GET['amount'])) ? $_GET['amount'] : 4000000;
$zipcode = (isset($_GET['zipcode'])) ? $_GET['zipcode'] : 2730;

$callback = file_get_contents("https://bolighed.dk/api/external/market/propertyforsale/?limit=5&kontantpris__lte=".$amount."&postnummer=".$zipcode);

$callbackJson = json_decode($callback);

$json1 = array(
    "type"=>"card",
    "cards"=>array()
);

foreach ($callbackJson->results as $bolig) {
    array_push($json1["cards"], array(
        "text"=>$bolig->kontantpris . " kr.",
        "title"=>$bolig->address_text .', '. $bolig->postnummernavn,
        "image_source_url"=>$bolig->billeder[0]->url,        
        "buttons"=>array(
            array(
                "is_active"=>true,
                "title"=>"Se mere",
                "options"=>array(
                    "message"=>"Hello"
                ),
                "type"=>"postback"
            )
        ),
        "image_destination_url"=>$bolig->url
    ));
}

print_r(json_encode($json1, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE ));

?>